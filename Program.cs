﻿using Queue;

try
{
    // khởi tạo queue có 5 phần tử
    QueueDemo queue = new QueueDemo(5);
    // thêm vào queue
    queue.EnQueue(1);
    queue.EnQueue(2);
    queue.EnQueue(3);
    queue.EnQueue(4);
    queue.EnQueue(5);

    // lấy item trong queue ra
    Console.WriteLine("DeQueue: " + queue.DeQueue());
    Console.WriteLine("DeQueue: " + queue.DeQueue());

    // item tiếp theo lấy ra queue
    //Console.WriteLine("Peek: " + queue.Peek());
    Console.WriteLine("DeQueue: " + queue.DeQueue());
    Console.WriteLine("DeQueue: " + queue.DeQueue());
    Console.WriteLine("DeQueue: " + queue.DeQueue());
    

    // thêm 1 số nữa vào queue
    queue.EnQueue(6);
    // bốc item vừa thêm ra
    Console.WriteLine("DeQueue: " + queue.DeQueue());
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}
