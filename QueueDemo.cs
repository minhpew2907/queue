﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Queue
{
    public class QueueDemo
    {
        public int[] Items;
        public int MaxQueue;
        public int IndexOfLastItemInQueue;

        // Constructor khởi tạo queue với số lượng item và gán IndexOfLastItemInQueue = -1
        public QueueDemo(int maxQueue)
        {
            MaxQueue = maxQueue;
            Items = new int[MaxQueue];
            IndexOfLastItemInQueue = -1; // Đánh dấu Queue là trống.
        }

        // Thêm một phần tử vào hàng đợi
        public void EnQueue(int item)
        {
            if (IsFull())
            {
                throw new Exception("Queue is full");
            }
            IndexOfLastItemInQueue++;
            Items[IndexOfLastItemInQueue] = item;
        }

        // Lấy và xóa phần tử đầu hàng đợi
        public int DeQueue()
        {
            if (IsEmpty())
            {
                throw new Exception("Queue is empty");
            }
            int item = Items[0];
            Items[0] = item + 1;
            IndexOfLastItemInQueue--;
            return item;
        }

        // Trả về giá trị của phần tử đầu hàng đợi mà không xóa nó
        public int indexOfLastItemInQueue()
        {
            if (IsEmpty())
            {
                throw new Exception("Queue is empty");
            }
            return Items[IndexOfLastItemInQueue];
        }

        // Kiểm tra xem hàng đợi có rỗng hay không
        public bool IsEmpty() => IndexOfLastItemInQueue == -1;

        // Kiểm tra xem hàng đợi đã đầy hay không
        public bool IsFull() => IndexOfLastItemInQueue == MaxQueue - 1;

        // Đếm số lượng phần tử trong hàng đợi
        public int Count() => IndexOfLastItemInQueue + 1;

    }
}
